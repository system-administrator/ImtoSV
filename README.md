# Image to Slideshow Vido (ImtoSV)

## The pipeline
1. Create slides in beamer or whatever, and generate a PDF.
2. Make high-res images from PDF, for example:
  * `convert -resize 1920x1080 -density 300 presentation.pdf ./tmp/slides-%02d.jpg`
3. Use ImtoSV.py to create the slideshow. This requires ffmpeg to be installed. For example:
  * `python ImtoSV.py slideshow-from-folder --folder ./tmp/ --repeated_slide ./tmp/slides-02.jpg
`
  * Default output is named `./out.mp4`.
  * The `-h` help output is at the bottom
<!-- 3. Import images into ffDiaporama (Maybe now OpenShot, maybe avconv or ffmpeg) -->


## Fonts

1. The presentation uses _Roboto Slab_ as serif typeface, and _Fira Sans_ as sans-serif typeface.
2. Their font files are available in the _resources/fonts/_ directory.
3. These font files must be installed in the font manager of the local machine before compilation for the Beamer source is attempted.
4. Advice on installing fonts at http://askubuntu.com/questions/18357/how-to-install-otf-fonts - copy the files to a certain folder, then update the font cache.


## Possible improvements
1. Add slide on first blind student graduating with Masters degree (Willem Venter)
3. Add slide on Willem Visser A2 rating
5. Centre no place like 212.254 when using the slide
6. Positioning in Master the Mainframe boxes - when adding dates
8. Refine \disable command by providing explicit flags for CS-SCREEN and OPEN-DAY to avoid to-and-fro editing


## Output of `python create_slidevideo.py -h`

```
$ python create_slidevideo.py -h

usage: ImtoSV.py [-h] [-c CONF] [-f FOLDER] [-rp REPEATED_SLIDE]
                 [-sd SLIDE_DUR] [-fd FADE_DUR] [-fo FILE_NAME] [-e EXTENSION]
                 {create-config,slideshow-from-folder,slideshow-from-config}

A slide show creator, from pictures having uniform size and format. Uses
ffmpeg. There is a default fade in and fade out effect that you will need to
edit code to change.

positional arguments:
  {create-config,slideshow-from-folder,slideshow-from-config}
                        The mode in which you want to the script to operate.
                        create-config, will create a .conf file with the
                        default values for the slideshow. You can then edit
                        that config for custom times. slideshow-from-folder,
                        this is an easy way to just create a default slideshow
                        from a dir of files. slideshow-from-conf, here a
                        config file can be read in

optional arguments:
  -h, --help            show this help message and exit
  -c CONF, --conf CONF  The location of the conf file, Final / expected.
  -f FOLDER, --folder FOLDER
                        The location of the images, Final / expected.
  -rp REPEATED_SLIDE, --repeated_slide REPEATED_SLIDE
                        Optional, name of the image you want repeated between
                        each slide. Does nothing when reading config
  -sd SLIDE_DUR, --slide_dur SLIDE_DUR
                        Duration each slide will display for. Does nothing
                        when reading config
  -fd FADE_DUR, --fade_dur FADE_DUR
                        Duration of the fade-out and fade-in effect. Does
                        nothing when reading config
  -fo FILE_NAME, --file_name FILE_NAME
                        The output filename, with ext
  -e EXTENSION, --extension EXTENSION
                        Extension for input files. Does nothing when reading
                        config

```
